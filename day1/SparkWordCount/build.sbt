name := "WordCountScala"

version := "0.8"

scalaVersion := "2.11.8"


val sparkVersion = "2.1.1"


libraryDependencies ++= Seq(

  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion,
  "org.apache.kafka" % "kafka-clients" % "0.11.0.1"
)

scalacOptions ++= Seq("-encoding", "UTF-8")

