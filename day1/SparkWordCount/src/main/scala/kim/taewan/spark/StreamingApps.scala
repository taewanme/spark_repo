package kim.taewan.spark


import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent


/**
  * Created by taewan on 2018. 6. 13..
  */
object  StreamingApps {
  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setAppName("KafkaSample")

    val sc = new SparkContext(conf)

    //코드 추가
    // context 설정 정보

    ////

    //연결 정보 설정
    //코드 추가

    ////

    //토픽 설정
    //코드 추가

    ////

    val ds = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, params))

    //ds의 유입을 flatmap으로 wordcount
    //코드 추가

    ////

    ssc.start
    ssc.awaitTermination()
  }
}
