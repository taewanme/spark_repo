package kim.taewan.spark

import org.apache.spark._
import org.apache.log4j.{LogManager, Logger}

/**
  * Created by taewan on 2018. 6. 10..
  */
object SparkWordCount {

  def main(args: Array[String]) {

    val logger: Logger = LogManager.getLogger("kim.taewan.spark.ScalaWordCount")

    logger.info("Initializing Context")
    val conf = new SparkConf().setAppName("Spark Word Count")
    val sc = new SparkContext(conf)

    val inputRDD =  sc.textFile(args(0))
    val words = inputRDD.flatMap(line => line.split(" "))
    // Transform into word and count.
    val counts = words.map(word => (word, 1)).reduceByKey{case (x, y) => x + y}
    // Save the word count back out to a text file, causing evaluation.
    counts.saveAsTextFile(args(1))
  }
}
