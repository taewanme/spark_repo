import sys
from operator import add
from pyspark import SparkContext



if __name__ == "__main__":
    sc = SparkContext(appName = "Python Word Count")

    lines = sc.textFile(sys.argv[1])
    counts = lines.flatMap(lambda x: x.split(' ')) \
                  .map(lambda x: (x, 1)) \
                  .reduceByKey(add)
    counts.saveAsTextFile(sys.argv[2])
    sc.stop()
